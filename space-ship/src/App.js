import logo from './logo.svg';
import './App.css';
import Card from "./Components/Card";
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function App() {
  return (
    <div className='App'>
       <ToastContainer position='top-center' autoClose={1000} />
    <Card 
    title='Space Ship'
    imageUrl='https://cdn.pixabay.com/photo/2017/08/30/01/05/milky-way-2695569_960_720.jpg'
   // profile='https://cdn.pixabay.com/photo/2017/08/30/01ps://www.google.com/imgres?imgurl=https%3A%2F%2Fimg.freepik.com%2Fpremium-photo%2Ff/05/milky-way-2695569_960_720.jpg'htt
   profile='https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/0ce19f9f-888c-4a80-8d2a-ba2c20e82b58/d1vjd3m-1807e7d2-5480-4297-a83c-776ed178f557.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzBjZTE5ZjlmLTg4OGMtNGE4MC04ZDJhLWJhMmMyMGU4MmI1OFwvZDF2amQzbS0xODA3ZTdkMi01NDgwLTQyOTctYTgzYy03NzZlZDE3OGY1NTcuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.l7EXQtTYcg0P3vPTSUge5gW7aV_kEuh-1QViMHkROlo'
    body='Translating Ideas to Reality - ' 
    body1='#REACT, #JAVASCRIPT, #CSS, and #UXDesign'
    place='North America & Europe'
    email='hanservices.com'
    date='Joined March 2009'
    
    />
    
 </div>
   
  );
}

export default App;
