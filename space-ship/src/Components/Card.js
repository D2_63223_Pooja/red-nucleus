import React, { useState } from 'react'
import { toast } from 'react-toastify'
import '../Card.css'


function Card({title,imageUrl,profile,body,body1,place,email,date}) {
   
    const [state,setState] = useState(false);

     const follow = ()=>{
     setState(!state)
     if(!state)
     toast.success('Started following Space Ship!!')
     }

    

  return (
    
    <div className='card-container'>
            <div className="image-container">
                <img src={imageUrl} alt='' /> 
            </div>
            <div className="profile-container">
                <img src={profile} alt='' />
            </div>
            <div className="card-btn">
            <button onClick={follow}>
                {state ? 'Unfollow' : 'Follow'}
            </button>
            </div>
            <div className="card-title">
                <h3>{title}</h3>
            </div>
            <div>
                <p className="card-body">{body}
                   <span className="card-body1">{body1}</span>
                </p>
            </div>
            <div className="card-footer">
                <span>{place}</span>
                <span>{email}</span>
                <span>{date}</span>
            </div>
        </div>

     
  )
}

export default Card