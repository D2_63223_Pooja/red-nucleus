
## Install Node JS
Refer to https://nodejs.org/en/ to install nodejs

## Install create-react-app
Install create-react-app npm package globally. This will help to easily run the project and also build the source files easily. Use the following command to install create-react-app

npm install -g create-react-app

## Packages
 yarn add react-router-dom react-toastify

## Cloning and Running the Application in local
Clone the project into local

Install all the npm packages. Go into the project folder and type the following command to install all npm packages
npm install 

In order to run the application Type the following command
npm start

The Application Runs on localhost:3000

## Application design
- src
  - contains the source code of the application
  - directory structure
    - components
      - reusables components will be placed here
 
  
   Card.css file is used to style the web page.It contains decorative features like font,color,size,padding,margin etc 

## Resources
create-react-app : The following link has all the commands that can be used with create-react-app https://github.com/facebook/create-react-app


## react-toastify

- https://fkhadra.github.io/react-toastify/introduction/


